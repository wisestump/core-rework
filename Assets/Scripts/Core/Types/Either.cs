using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace Core.Types
{
    public static class Either
    {
        public static Either<L, R> Left<L, R>(L left) => new Either<L, R>(left);
        public static Either<L, R> Right<L, R>(R right) => new Either<L, R>(right);
    }

    public readonly struct Either<L, R> : IEquatable<Either<L, R>>
    {
        public struct Union
        {
            public L Left;
            public R Right;
        }

        internal readonly Union union;
        internal readonly bool isRight;

        internal Either(R right)
        {
            this.isRight = true;
            this.union.Left = default(L);
            this.union.Right = right;
        }

        internal Either(L left)
        {
            this.isRight = false;
            this.union.Right = default(R);
            this.union.Left = left;
        }

        /// <summary>
        /// Is the Either in a Right state?
        /// </summary>
        public bool IsRight => isRight;

        /// <summary>
        /// Is the Either in a Left state?
        /// </summary>
        public bool IsLeft => !isRight;

        public L Left
        {
            get
            {
                if (IsRight)
                {
                    throw new Exception("Either doesn't hold Left");
                }
                return union.Left;
            }
        }

        public R Right
        {
            get
            {
                if (IsLeft)
                {
                    throw new Exception("Either doesn't hold Right");
                }
                return union.Right;
            }
        }

        public override bool Equals(object obj)
        {
            return obj is Either<L, R> either && this.Equals(either);
        }

        public bool Equals(Either<L, R> other)
        {
            return isRight == other.isRight && 
                (isRight && EqualityComparer<R>.Default.Equals(union.Right, other.union.Right) ||
                !isRight && EqualityComparer<L>.Default.Equals(union.Left, other.union.Left));
        }

        public override int GetHashCode()
        {
            int hashCode = 1657436122;
            hashCode = hashCode * -1521134295 + isRight.GetHashCode();
            if (isRight)
            {
                hashCode = hashCode * -1521134295 + EqualityComparer<R>.Default.GetHashCode(union.Right);
            }
            else
            {
                hashCode = hashCode * -1521134295 + EqualityComparer<L>.Default.GetHashCode(union.Left);
            }
            return hashCode;
        }

        public static bool operator ==(Either<L, R> left, Either<L, R> right)
        {
            return left.Equals(right);
        }

        public static bool operator !=(Either<L, R> left, Either<L, R> right)
        {
            return !(left == right);
        }
    }
}
