namespace Core
{
    public class Id<T> : ICommand<T, T>
    {
        public T Execute(T param) => param;

        public static Id<T> Instance { get; } = new Id<T>();
    }

    class Return<TP, TR> : ICommand<TP, TR>
    {
        private readonly TR result;

        public Return(TR result)
        {
            this.result = result;
        }

        public TR Execute(TP param) => result;
    }
}
