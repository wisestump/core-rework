using System;
using Cysharp.Threading.Tasks;
using Core.Types;
using Core.Async;

namespace Core
{
    class BranchingDecorator<TParam, TResult> : ICommand<TParam, Either<TResult, TResult>>
    {
        private readonly ICommand<TParam, TResult> command;
        private readonly Predicate<TResult> condition;

        public BranchingDecorator(ICommand<TParam, TResult> command, Predicate<TResult> condition)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.condition = condition ?? throw new ArgumentNullException(nameof(condition));
        }

        public Either<TResult, TResult> Execute(TParam param)
        {
            var result = command.Execute(param);
            return condition(result) ?
                Either.Right<TResult, TResult>(result) :
                Either.Left<TResult, TResult>(result);
        }
    }

    class BranchingDecoratorAsync1<TParam, TResult> : ICommandAsync<TParam, Either<TResult, TResult>>
    {
        private readonly ICommandAsync<TParam, TResult> command;
        private readonly Predicate<TResult> condition;

        public BranchingDecoratorAsync1(ICommandAsync<TParam, TResult> command, Predicate<TResult> condition)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.condition = condition ?? throw new ArgumentNullException(nameof(condition));
        }

        public async UniTask<Either<TResult, TResult>> Execute(TParam param)
        {
            var result = await command.Execute(param);
            return condition(result) ?
                Either.Right<TResult, TResult>(result) :
                Either.Left<TResult, TResult>(result);
        }
    }

    class BranchingDecoratorAsync2<TParam, TResult> : ICommandAsync<TParam, Either<TResult, TResult>>
    {
        private readonly ICommandAsync<TParam, TResult> command;
        private readonly Func<TResult, UniTask<bool>> condition;

        public BranchingDecoratorAsync2(ICommandAsync<TParam, TResult> command, Func<TResult, UniTask<bool>> condition)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.condition = condition ?? throw new ArgumentNullException(nameof(condition));
        }

        public async UniTask<Either<TResult, TResult>> Execute(TParam param)
        {
            var result = await command.Execute(param);
            return await condition(result) ?
                Either.Right<TResult, TResult>(result) :
                Either.Left<TResult, TResult>(result);
        }
    }

    class BranchingDecoratorAsync3<TParam, TResult> : ICommandAsync<TParam, Either<TResult, TResult>>
    {
        private readonly ICommand<TParam, TResult> command;
        private readonly Func<TResult, UniTask<bool>> condition;

        public BranchingDecoratorAsync3(ICommand<TParam, TResult> command, Func<TResult, UniTask<bool>> condition)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.condition = condition ?? throw new ArgumentNullException(nameof(condition));
        }

        public async UniTask<Either<TResult, TResult>> Execute(TParam param)
        {
            var result = command.Execute(param);
            return await condition(result) ?
                Either.Right<TResult, TResult>(result) :
                Either.Left<TResult, TResult>(result);
        }
    }

}
