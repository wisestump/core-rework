using System;
using Cysharp.Threading.Tasks;
using Core.Async;

namespace Core
{
    class ResultConvertDecoratorBase<TParam, TResult, TNewResult>
    {
        private readonly Func<TResult, TNewResult> converter;

        public ResultConvertDecoratorBase(Func<TResult, TNewResult> converter)
        {
            this.converter = converter ?? throw new ArgumentNullException(nameof(converter));
        }

        protected TNewResult Apply(TResult result) => converter(result);
    }

    class ResultConvertDecorator<TParam, TResult, TNewResult> : ResultConvertDecoratorBase<TParam, TResult, TNewResult>, ICommand<TParam, TNewResult>
    {
        private readonly ICommand<TParam, TResult> command;

        public ResultConvertDecorator(ICommand<TParam, TResult> command, Func<TResult, TNewResult> converter) : base(converter)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
        }

        public TNewResult Execute(TParam param) => Apply(command.Execute(param));
    }

    class ResultConvertDecoratorAsync1<TParam, TResult, TNewResult> :
        ResultConvertDecoratorBase<TParam, TResult, TNewResult>,
        ICommandAsync<TParam, TNewResult>
    {
        private readonly ICommandAsync<TParam, TResult> command;

        public ResultConvertDecoratorAsync1(ICommandAsync<TParam, TResult> command, Func<TResult, TNewResult> converter)
            : base(converter)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
        }

        public async UniTask<TNewResult> Execute(TParam param) => Apply(await command.Execute(param));
    }

    class ResultConvertDecoratorAsync2<TParam, TResult, TNewResult> :
        ICommandAsync<TParam, TNewResult>
    {
        private readonly ICommandAsync<TParam, TResult> command;
        private readonly Func<TResult, UniTask<TNewResult>> converter;

        public ResultConvertDecoratorAsync2(ICommandAsync<TParam, TResult> command, Func<TResult, UniTask<TNewResult>> converter)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.converter = converter ?? throw new ArgumentNullException(nameof(converter));
        }

        public async UniTask<TNewResult> Execute(TParam param) => await converter(await command.Execute(param));
    }

    class ResultConvertDecoratorAsync3<TParam, TResult, TNewResult> :
        ICommandAsync<TParam, TNewResult>
    {
        private readonly ICommand<TParam, TResult> command;
        private readonly Func<TResult, UniTask<TNewResult>> converter;

        public ResultConvertDecoratorAsync3(ICommand<TParam, TResult> command, Func<TResult, UniTask<TNewResult>> converter)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.converter = converter ?? throw new ArgumentNullException(nameof(converter));
        }

        public async UniTask<TNewResult> Execute(TParam param) => await converter(command.Execute(param));
    }
}
