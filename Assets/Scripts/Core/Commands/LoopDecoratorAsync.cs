using System.Threading;
using Cysharp.Threading.Tasks;
using Core.Async;

namespace Core
{
    class LoopDecoratorAsync<TParam, TResult> : ICommandAsync<TParam, TResult>
    {
        private readonly ICommandAsync<TParam, TResult> command;
        private readonly CancellationToken cancellationToken;

        public LoopDecoratorAsync(ICommandAsync<TParam, TResult> command, CancellationToken cancellationToken)
        {
            this.command = command;
            this.cancellationToken = cancellationToken;
        }

        public async UniTask<TResult> Execute(TParam param)
        {
            while (true)
            {
                await command.Execute(param).AttachExternalCancellation(cancellationToken);
            }
        }
    }
}
