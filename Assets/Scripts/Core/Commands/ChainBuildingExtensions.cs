using System;
using System.Collections;
using System.Collections.Generic;
using Core.Types;

namespace Core
{
    public static class ChainBuildingExtensions
    {
        public static ICommand<TP, TR2> Next<TP, TR1, TR2>(this ICommand<TP, TR1> first, ICommand<TR1, TR2> second)
        {
            return new ResultConvertDecorator<TP, TR1, TR2>(first, second.Execute);
        }

        public static ICommand<TP, Either<TR, TR>> If<TP, TR>(this ICommand<TP, TR> command, Predicate<TR> condition)
        {
            return new BranchingDecorator<TP, TR>(command, condition);
        }

        public static ICommand<TP, Either<TR1, TR2>> Then<TP, TR1, TR2>(this ICommand<TP, Either<TR1, TR1>> command, ICommand<TR1, TR2> then)
        {
            return new EitherRightDecorator<TP, TR1, TR1, TR2>(command, then.Execute);
        }

        public static ICommand<TP, Either<TR1, TR2>> Then<TP, TR1, TR2>(
            this ICommand<TP, Either<Either<TR1, TR1>, TR2>> command, 
            ICommand<TR1, TR2> then)
        {
            var appliedThen =
                new EitherLeftDecorator<TP, Either<TR1, TR1>, Either<TR1, TR2>, TR2>(
                    command,
                    result => DecoratingRoutines.ApplyRight(result, then.Execute));

            return appliedThen.Convert(x => x.IsRight ? Either.Right<TR1, TR2>(x.Right) : x.Left);
        }

        public static ICommand<TP, TR2> Else<TP, TR1, TR2>(this ICommand<TP, Either<TR1, TR2>> command, ICommand<TR1, TR2> @else)
        {
            return new MergingDecorator<TP, TR1, TR2, TR2>(command, @else.Execute, Id<TR2>.Instance.Execute);
        }

        public static ICommand<TP, Either<Either<TR1, TR1>, TR2>> ElseIf<TP, TR1, TR2>(this ICommand<TP, Either<TR1, TR2>> command, Predicate<TR1> condition)
        {
            return new EitherLeftDecorator<TP, TR1, Either<TR1, TR1>, TR2>(
                command,
                result => DecoratingRoutines.Branch(condition, result));
        }

        public static ICommand<TP, TR2> Convert<TP, TR1, TR2>(this ICommand<TP, TR1> command, Func<TR1, TR2> converter)
        {
            return new ResultConvertDecorator<TP, TR1, TR2>(command, converter);
        }

        public static ICommand<TP, Unit> ToUnit<TP, TR>(this ICommand<TP, TR> command)
        {
            return command.Convert(_ => Unit.Default);
        }

        public static ICommand<Unit, TR> WithArgument<TP, TR>(this ICommand<TP, TR> command, TP arg)
        {
            return new Return<Unit, TP>(arg).Next(command);
        }
    }
}
