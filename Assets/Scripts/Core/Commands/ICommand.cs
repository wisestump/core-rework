using Cysharp.Threading.Tasks;

namespace Core
{
    public interface ICommand<TParam, TResult>
    {
        TResult Execute(TParam param);
    }
}

namespace Core.Async
{
    public interface ICommandAsync<TParam, TResult> : ICommand<TParam, UniTask<TResult>>
    {
    }
}
