using System;
using Cysharp.Threading.Tasks;
using Core.Types;
using Core.Async;

namespace Core
{
    class MergingDecorator<TParam, TL, TR, TResult> : ICommand<TParam, TResult>
    {
        private readonly ICommand<TParam, Either<TL, TR>> command;
        private readonly Func<TL, TResult> ifLeft;
        private readonly Func<TR, TResult> ifRight;

        public MergingDecorator(ICommand<TParam, Either<TL, TR>> command, Func<TL, TResult> ifLeft, Func<TR, TResult> ifRight)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.ifLeft = ifLeft ?? throw new ArgumentNullException(nameof(ifLeft));
            this.ifRight = ifRight ?? throw new ArgumentNullException(nameof(ifRight));
        }

        public TResult Execute(TParam param)
        {
            var result = command.Execute(param);
            if (result.IsLeft)
            {
                return ifLeft(result.Left);
            }
            else
            {
                return ifRight(result.Right);
            }
        }
    }

    class MergingDecoratorAsync1<TParam, TL, TR, TResult> : ICommandAsync<TParam, TResult>
    {
        private readonly ICommandAsync<TParam, Either<TL, TR>> command;
        private readonly Func<TL, TResult> ifLeft;
        private readonly Func<TR, TResult> ifRight;

        public MergingDecoratorAsync1(ICommandAsync<TParam, Either<TL, TR>> command, Func<TL, TResult> ifLeft, Func<TR, TResult> ifRight)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.ifLeft = ifLeft ?? throw new ArgumentNullException(nameof(ifLeft));
            this.ifRight = ifRight ?? throw new ArgumentNullException(nameof(ifRight));
        }

        public async UniTask<TResult> Execute(TParam param)
        {
            var result = await command.Execute(param);
            if (result.IsLeft)
            {
                return ifLeft(result.Left);
            }
            else
            {
                return ifRight(result.Right);
            }
        }
    }

    class MergingDecoratorAsync2<TParam, TL, TR, TResult> : ICommandAsync<TParam, TResult>
    {
        private readonly ICommandAsync<TParam, Either<TL, TR>> command;
        private readonly Func<TL, UniTask<TResult>> ifLeft;
        private readonly Func<TR, TResult> ifRight;

        public MergingDecoratorAsync2(ICommandAsync<TParam, Either<TL, TR>> command, Func<TL, UniTask<TResult>> ifLeft, Func<TR, TResult> ifRight)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.ifLeft = ifLeft ?? throw new ArgumentNullException(nameof(ifLeft));
            this.ifRight = ifRight ?? throw new ArgumentNullException(nameof(ifRight));
        }

        public async UniTask<TResult> Execute(TParam param)
        {
            var result = await command.Execute(param);
            if (result.IsLeft)
            {
                return await ifLeft(result.Left);
            }
            else
            {
                return ifRight(result.Right);
            }
        }
    }

    class MergingDecoratorAsync3<TParam, TL, TR, TResult> : ICommandAsync<TParam, TResult>
    {
        private readonly ICommandAsync<TParam, Either<TL, TR>> command;
        private readonly Func<TL, UniTask<TResult>> ifLeft;
        private readonly Func<TR, UniTask<TResult>> ifRight;

        public MergingDecoratorAsync3(ICommandAsync<TParam, Either<TL, TR>> command, Func<TL, UniTask<TResult>> ifLeft, Func<TR, UniTask<TResult>> ifRight)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.ifLeft = ifLeft ?? throw new ArgumentNullException(nameof(ifLeft));
            this.ifRight = ifRight ?? throw new ArgumentNullException(nameof(ifRight));
        }

        public async UniTask<TResult> Execute(TParam param)
        {
            var result = await command.Execute(param);
            if (result.IsLeft)
            {
                return await ifLeft(result.Left);
            }
            else
            {
                return await ifRight(result.Right);
            }
        }
    }

    class MergingDecoratorAsync4<TParam, TL, TR, TResult> : ICommandAsync<TParam, TResult>
    {
        private readonly ICommandAsync<TParam, Either<TL, TR>> command;
        private readonly Func<TL, TResult> ifLeft;
        private readonly Func<TR, UniTask<TResult>> ifRight;

        public MergingDecoratorAsync4(ICommandAsync<TParam, Either<TL, TR>> command, Func<TL, TResult> ifLeft, Func<TR, UniTask<TResult>> ifRight)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.ifLeft = ifLeft ?? throw new ArgumentNullException(nameof(ifLeft));
            this.ifRight = ifRight ?? throw new ArgumentNullException(nameof(ifRight));
        }

        public async UniTask<TResult> Execute(TParam param)
        {
            var result = await command.Execute(param);
            if (result.IsLeft)
            {
                return ifLeft(result.Left);
            }
            else
            {
                return await ifRight(result.Right);
            }
        }
    }

    class MergingDecoratorAsync5<TParam, TL, TR, TResult> : ICommandAsync<TParam, TResult>
    {
        private readonly ICommand<TParam, Either<TL, TR>> command;
        private readonly Func<TL, UniTask<TResult>> ifLeft;
        private readonly Func<TR, TResult> ifRight;

        public MergingDecoratorAsync5(ICommand<TParam, Either<TL, TR>> command, Func<TL, UniTask<TResult>> ifLeft, Func<TR, TResult> ifRight)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.ifLeft = ifLeft ?? throw new ArgumentNullException(nameof(ifLeft));
            this.ifRight = ifRight ?? throw new ArgumentNullException(nameof(ifRight));
        }

        public async UniTask<TResult> Execute(TParam param)
        {
            var result = command.Execute(param);
            if (result.IsLeft)
            {
                return await ifLeft(result.Left);
            }
            else
            {
                return ifRight(result.Right);
            }
        }
    }

    class MergingDecoratorAsync6<TParam, TL, TR, TResult> : ICommandAsync<TParam, TResult>
    {
        private readonly ICommand<TParam, Either<TL, TR>> command;
        private readonly Func<TL, UniTask<TResult>> ifLeft;
        private readonly Func<TR, UniTask<TResult>> ifRight;

        public MergingDecoratorAsync6(ICommand<TParam, Either<TL, TR>> command, Func<TL, UniTask<TResult>> ifLeft, Func<TR, UniTask<TResult>> ifRight)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.ifLeft = ifLeft ?? throw new ArgumentNullException(nameof(ifLeft));
            this.ifRight = ifRight ?? throw new ArgumentNullException(nameof(ifRight));
        }

        public async UniTask<TResult> Execute(TParam param)
        {
            var result = command.Execute(param);
            if (result.IsLeft)
            {
                return await ifLeft(result.Left);
            }
            else
            {
                return await ifRight(result.Right);
            }
        }
    }

    class MergingDecoratorAsync7<TParam, TL, TR, TResult> : ICommandAsync<TParam, TResult>
    {
        private readonly ICommand<TParam, Either<TL, TR>> command;
        private readonly Func<TL, TResult> ifLeft;
        private readonly Func<TR, UniTask<TResult>> ifRight;

        public MergingDecoratorAsync7(ICommand<TParam, Either<TL, TR>> command, Func<TL, TResult> ifLeft, Func<TR, UniTask<TResult>> ifRight)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.ifLeft = ifLeft ?? throw new ArgumentNullException(nameof(ifLeft));
            this.ifRight = ifRight ?? throw new ArgumentNullException(nameof(ifRight));
        }

        public async UniTask<TResult> Execute(TParam param)
        {
            var result = command.Execute(param);
            if (result.IsLeft)
            {
                return ifLeft(result.Left);
            }
            else
            {
                return await ifRight(result.Right);
            }
        }
    }

}
