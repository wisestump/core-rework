using System;
using Cysharp.Threading.Tasks;
using Core.Types;
using Core.Async;

namespace Core
{
    class EitherRightDecorator<TParam, TL, TR, TRNew> : ICommand<TParam, Either<TL, TRNew>>
    {
        private readonly ICommand<TParam, Either<TL, TR>> command;
        private readonly Func<TR, TRNew> func;

        public EitherRightDecorator(ICommand<TParam, Either<TL, TR>> command, Func<TR, TRNew> func)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.func = func ?? throw new ArgumentNullException(nameof(func));
        }

        public Either<TL, TRNew> Execute(TParam param)
        {
            var result = command.Execute(param);
            if (result.IsRight)
            {
                return Either.Right<TL, TRNew>(func(result.Right));
            }
            else
            {
                return Either.Left<TL, TRNew>(result.Left);
            }
        }
    }
    class EitherRightDecoratorAsync1<TParam, TL, TR, TRNew> : ICommandAsync<TParam, Either<TL, TRNew>>
    {
        private readonly ICommandAsync<TParam, Either<TL, TR>> command;
        private readonly Func<TR, TRNew> func;

        public EitherRightDecoratorAsync1(ICommandAsync<TParam, Either<TL, TR>> command, Func<TR, TRNew> func)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.func = func ?? throw new ArgumentNullException(nameof(func));
        }

        public async UniTask<Either<TL, TRNew>> Execute(TParam param)
        {
            var result = await command.Execute(param);
            if (result.IsRight)
            {
                return Either.Right<TL, TRNew>(func(result.Right));
            }
            else
            {
                return Either.Left<TL, TRNew>(result.Left);
            }
        }
    }

    class EitherRightDecoratorAsync2<TParam, TL, TR, TRNew> : ICommandAsync<TParam, Either<TL, TRNew>>
    {
        private readonly ICommandAsync<TParam, Either<TL, TR>> command;
        private readonly Func<TR, UniTask<TRNew>> func;

        public EitherRightDecoratorAsync2(ICommandAsync<TParam, Either<TL, TR>> command, Func<TR, UniTask<TRNew>> func)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.func = func;
        }

        public async UniTask<Either<TL, TRNew>> Execute(TParam param)
        {
            var result = await command.Execute(param);
            if (result.IsRight)
            {
                return Either.Right<TL, TRNew>(await func(result.Right));
            }
            else
            {
                return Either.Left<TL, TRNew>(result.Left);
            }
        }
    }

    class EitherRightDecoratorAsync3<TParam, TL, TR, TRNew> : ICommandAsync<TParam, Either<TL, TRNew>>
    {
        private readonly ICommand<TParam, Either<TL, TR>> command;
        private readonly Func<TR, UniTask<TRNew>> func;

        public EitherRightDecoratorAsync3(ICommand<TParam, Either<TL, TR>> command, Func<TR, UniTask<TRNew>> func)
        {
            this.command = command;
            this.func = func;
        }

        public async UniTask<Either<TL, TRNew>> Execute(TParam param)
        {
            var result = command.Execute(param);
            if (result.IsRight)
            {
                return Either.Right<TL, TRNew>(await func(result.Right));
            }
            else
            {
                return Either.Left<TL, TRNew>(result.Left);
            }
        }
    }
}
