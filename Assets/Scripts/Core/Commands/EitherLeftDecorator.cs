using System;
using Cysharp.Threading.Tasks;
using Core.Types;
using Core.Async;

namespace Core
{
    class EitherLeftDecoratorBase<TParam, TL, TLNew, TR> 
    {
        private readonly Func<TL, TLNew> func;

        public EitherLeftDecoratorBase(Func<TL, TLNew> func)
        {
            this.func = func ?? throw new ArgumentNullException(nameof(func));
        }

        protected Either<TLNew, TR> Apply(Either<TL, TR> result) => DecoratingRoutines.ApplyLeft(result, func);
    }

    class EitherLeftDecorator<TParam, TL, TLNew, TR> : EitherLeftDecoratorBase<TParam, TL, TLNew, TR>, ICommand<TParam, Either<TLNew, TR>>
    {
        private readonly ICommand<TParam, Either<TL, TR>> command;

        public EitherLeftDecorator(ICommand<TParam, Either<TL, TR>> command, Func<TL, TLNew> func)
            : base(func)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
        }

        public Either<TLNew, TR> Execute(TParam param) => Apply(command.Execute(param));
    }

    class EitherLeftDecoratorAsync1<TParam, TL, TLNew, TR> :
        EitherLeftDecoratorBase<TParam, TL, TLNew, TR>,
        ICommandAsync<TParam, Either<TLNew, TR>>
    {
        private readonly ICommandAsync<TParam, Either<TL, TR>> command;

        public EitherLeftDecoratorAsync1(ICommandAsync<TParam, Either<TL, TR>> command, Func<TL, TLNew> func)
            : base(func)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
        }

        public async UniTask<Either<TLNew, TR>> Execute(TParam param) => Apply(await command.Execute(param));
    }

    class EitherLeftDecoratorAsync2<TParam, TL, TLNew, TR> : ICommandAsync<TParam, Either<TLNew, TR>>
    {
        private readonly ICommandAsync<TParam, Either<TL, TR>> command;
        private readonly Func<TL, UniTask<TLNew>> func;

        public EitherLeftDecoratorAsync2(ICommandAsync<TParam, Either<TL, TR>> command, Func<TL, UniTask<TLNew>> func)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.func = func;
        }

        public async UniTask<Either<TLNew, TR>> Execute(TParam param) =>
            await DecoratingRoutines.ApplyLeftAsync(await command.Execute(param), func);
    }

    class EitherLeftDecoratorAsync3<TParam, TL, TLNew, TR> : ICommandAsync<TParam, Either<TLNew, TR>>
    {
        private readonly ICommand<TParam, Either<TL, TR>> command;
        private readonly Func<TL, UniTask<TLNew>> func;

        public EitherLeftDecoratorAsync3(ICommand<TParam, Either<TL, TR>> command, Func<TL, UniTask<TLNew>> func)
        {
            this.command = command ?? throw new ArgumentNullException(nameof(command));
            this.func = func ?? throw new ArgumentNullException(nameof(func));
        }

        public async UniTask<Either<TLNew, TR>> Execute(TParam param) =>
            await DecoratingRoutines.ApplyLeftAsync(command.Execute(param), func);
    }

}
