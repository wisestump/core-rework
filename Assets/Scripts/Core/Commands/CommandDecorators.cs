using System;
using Cysharp.Threading.Tasks;
using Core.Types;

namespace Core
{
    static class DecoratingRoutines
    {
        public static Either<TLNew, TR> ApplyLeft<TL, TLNew, TR>(Either<TL, TR> result, Func<TL, TLNew> func)
        {
            if (result.IsLeft)
            {
                return Either.Left<TLNew, TR>(func(result.Left));
            }
            else
            {
                return Either.Right<TLNew, TR>(result.Right);
            }
        }

        public static async UniTask<Either<TLNew, TR>> ApplyLeftAsync<TL, TLNew, TR>(Either<TL, TR> result, Func<TL, UniTask<TLNew>> func)
        {
            if (result.IsLeft)
            {
                return Either.Left<TLNew, TR>(await func(result.Left));
            }
            else
            {
                return Either.Right<TLNew, TR>(result.Right);
            }
        }

        public static Either<TL, TRNew> ApplyRight<TL, TR, TRNew>(Either<TL, TR> result, Func<TR, TRNew> func)
        {
            if (result.IsRight)
            {
                return Either.Right<TL, TRNew>(func(result.Right));
            }
            else
            {
                return Either.Left<TL, TRNew>(result.Left);
            }
        }

        public static async UniTask<Either<TL, TRNew>> ApplyRightAsync<TL, TR, TRNew>(Either<TL, TR> result, Func<TR, UniTask<TRNew>> func)
        {
            if (result.IsRight)
            {
                return Either.Right<TL, TRNew>(await func(result.Right));
            }
            else
            {
                return Either.Left<TL, TRNew>(result.Left);
            }
        }

        public static Either<TResult, TResult> Branch<TResult>(Predicate<TResult> condition, TResult result) =>
            condition(result) ? Either.Right<TResult, TResult>(result) : Either.Left<TResult, TResult>(result);

        public static async UniTask<Either<TResult, TResult>> BranchAsync<TResult>(Func<TResult, UniTask<bool>> condition, TResult result) =>
            await condition(result) ? Either.Right<TResult, TResult>(result) : Either.Left<TResult, TResult>(result);
    }
}
