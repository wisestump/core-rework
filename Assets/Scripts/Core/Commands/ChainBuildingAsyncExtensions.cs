using System;
using Cysharp.Threading.Tasks;
using Core.Types;

namespace Core.Async
{
    public static class ChainBuildingAsyncExtensions
    {
        // Next

        public static ICommandAsync<TP, TR2> Next<TP, TR1, TR2>(this ICommandAsync<TP, TR1> first, ICommand<TR1, TR2> second)
        {
            return new ResultConvertDecoratorAsync1<TP, TR1, TR2>(first, second.Execute);
        }

        public static ICommandAsync<TP, TR2> Next<TP, TR1, TR2>(this ICommandAsync<TP, TR1> first, ICommandAsync<TR1, TR2> second)
        {
            return new ResultConvertDecoratorAsync2<TP, TR1, TR2>(first, second.Execute);
        }

        public static ICommandAsync<TP, TR2> Next<TP, TR1, TR2>(this ICommand<TP, TR1> first, ICommandAsync<TR1, TR2> second)
        {
            return new ResultConvertDecoratorAsync3<TP, TR1, TR2>(first, second.Execute);
        }

        // If

        public static ICommandAsync<TP, Either<TR, TR>> If<TP, TR>(this ICommandAsync<TP, TR> command, Predicate<TR> condition)
        {
            return new BranchingDecoratorAsync1<TP, TR>(command, condition);
        }

        public static ICommandAsync<TP, Either<TR, TR>> If<TP, TR>(this ICommandAsync<TP, TR> command, Func<TR, UniTask<bool>> condition)
        {
            return new BranchingDecoratorAsync2<TP, TR>(command, condition);
        }

        public static ICommandAsync<TP, Either<TR, TR>> If<TP, TR>(this ICommand<TP, TR> command, Func<TR, UniTask<bool>> condition)
        {
            return new BranchingDecoratorAsync3<TP, TR>(command, condition);
        }

        // Then

        public static ICommandAsync<TP, Either<TR1, TR2>> Then<TP, TR1, TR2>(this ICommandAsync<TP, Either<TR1, TR1>> command, ICommand<TR1, TR2> then)
        {
            return new EitherRightDecoratorAsync1<TP, TR1, TR1, TR2>(command, then.Execute);
        }

        public static ICommandAsync<TP, Either<TR1, TR2>> Then<TP, TR1, TR2>(this ICommandAsync<TP, Either<TR1, TR1>> command, ICommandAsync<TR1, TR2> then)
        {
            return new EitherRightDecoratorAsync2<TP, TR1, TR1, TR2>(command, then.Execute);
        }

        public static ICommandAsync<TP, Either<TR1, TR2>> Then<TP, TR1, TR2>(this ICommand<TP, Either<TR1, TR1>> command, ICommandAsync<TR1, TR2> then)
        {
            return new EitherRightDecoratorAsync3<TP, TR1, TR1, TR2>(command, then.Execute);
        }

        // Then inside if-else

        public static ICommandAsync<TP, Either<TR1, TR2>> Then<TP, TR1, TR2>(
            this ICommandAsync<TP, Either<Either<TR1, TR1>, TR2>> command,
            ICommandAsync<TR1, TR2> then)
        {
            var appliedThen =
                new EitherLeftDecoratorAsync2<TP, Either<TR1, TR1>, Either<TR1, TR2>, TR2>(
                    command,
                    async result => await DecoratingRoutines.ApplyRightAsync(result, then.Execute));

            return appliedThen.Convert(x => x.IsRight ? Either.Right<TR1, TR2>(x.Right) : x.Left);
        }

        public static ICommandAsync<TP, Either<TR1, TR2>> Then<TP, TR1, TR2>(
            this ICommand<TP, Either<Either<TR1, TR1>, TR2>> command,
            ICommandAsync<TR1, TR2> then)
        {
            var appliedThen =
                new EitherLeftDecoratorAsync3<TP, Either<TR1, TR1>, Either<TR1, TR2>, TR2>(
                    command,
                    result => DecoratingRoutines.ApplyRightAsync(result, then.Execute));

            return appliedThen.Convert(x => x.IsRight ? Either.Right<TR1, TR2>(x.Right) : x.Left);
        }

        public static ICommandAsync<TP, Either<TR1, TR2>> Then<TP, TR1, TR2>(
            this ICommandAsync<TP, Either<Either<TR1, TR1>, TR2>> command,
            ICommand<TR1, TR2> then)
        {
            var appliedThen =
                new EitherLeftDecoratorAsync1<TP, Either<TR1, TR1>, Either<TR1, TR2>, TR2>(
                    command,
                    result => DecoratingRoutines.ApplyRight(result, then.Execute));

            return appliedThen.Convert(x => x.IsRight ? Either.Right<TR1, TR2>(x.Right) : x.Left);
        }

        // Else

        public static ICommandAsync<TP, TR2> Else<TP, TR1, TR2>(this ICommandAsync<TP, Either<TR1, TR2>> command, ICommandAsync<TR1, TR2> @else)
        {
            return new MergingDecoratorAsync2<TP, TR1, TR2, TR2>(command, @else.Execute, Id<TR2>.Instance.Execute);
        }

        public static ICommandAsync<TP, TR2> Else<TP, TR1, TR2>(this ICommand<TP, Either<TR1, TR2>> command, ICommandAsync<TR1, TR2> @else)
        {
            return new MergingDecoratorAsync5<TP, TR1, TR2, TR2>(command, @else.Execute, Id<TR2>.Instance.Execute);
        }

        public static ICommandAsync<TP, TR2> Else<TP, TR1, TR2>(this ICommandAsync<TP, Either<TR1, TR2>> command, ICommand<TR1, TR2> @else)
        {
            return new MergingDecoratorAsync1<TP, TR1, TR2, TR2>(command, @else.Execute, Id<TR2>.Instance.Execute);
        }

        // Else if

        public static ICommandAsync<TP, Either<Either<TR1, TR1>, TR2>> ElseIf<TP, TR1, TR2>(this ICommandAsync<TP, Either<TR1, TR2>> command, Predicate<TR1> condition)
        {
            return new EitherLeftDecoratorAsync1<TP, TR1, Either<TR1, TR1>, TR2>(
                command,
                result => DecoratingRoutines.Branch(condition, result));
        }

        public static ICommandAsync<TP, Either<Either<TR1, TR1>, TR2>> ElseIf<TP, TR1, TR2>(this ICommandAsync<TP, Either<TR1, TR2>> command, Func<TR1, UniTask<bool>> condition)
        {
            return new EitherLeftDecoratorAsync2<TP, TR1, Either<TR1, TR1>, TR2>(
                command,
                result => DecoratingRoutines.BranchAsync(condition, result));
        }

        public static ICommandAsync<TP, Either<Either<TR1, TR1>, TR2>> ElseIf<TP, TR1, TR2>(this ICommand<TP, Either<TR1, TR2>> command, Func<TR1, UniTask<bool>> condition)
        {
            return new EitherLeftDecoratorAsync3<TP, TR1, Either<TR1, TR1>, TR2>(
                command,
                result => DecoratingRoutines.BranchAsync(condition, result));
        }

        // Convert

        public static ICommandAsync<TP, TR2> Convert<TP, TR1, TR2>(this ICommandAsync<TP, TR1> command, Func<TR1, UniTask<TR2>> converter)
        {
            return new ResultConvertDecoratorAsync2<TP, TR1, TR2>(command, converter);
        }

        public static ICommandAsync<TP, TR2> Convert<TP, TR1, TR2>(this ICommand<TP, TR1> command, Func<TR1, UniTask<TR2>> converter)
        {
            return new ResultConvertDecoratorAsync3<TP, TR1, TR2>(command, converter);
        }

        public static ICommandAsync<TP, TR2> Convert<TP, TR1, TR2>(this ICommandAsync<TP, TR1> command, Func<TR1, TR2> converter)
        {
            return new ResultConvertDecoratorAsync1<TP, TR1, TR2>(command, converter);
        }

        // To unit

        public static ICommandAsync<TP, Unit> ToUnit<TP, TR>(this ICommandAsync<TP, TR> command)
        {
            return command.Convert(_ => Unit.Default);
        }

        public static ICommandAsync<Unit, TR> WithArgument<TP, TR>(this ICommandAsync<TP, TR> command, TP arg)
        {
            return new Return<Unit, TP>(arg).Next(command);
        }

        // Loop

        public static ICommandAsync<TP, TR> Loop<TP, TR>(this ICommandAsync<TP, TR> command, System.Threading.CancellationToken cancellationToken)
        {
            return new LoopDecoratorAsync<TP, TR>(command, cancellationToken);
        }
    }
}
