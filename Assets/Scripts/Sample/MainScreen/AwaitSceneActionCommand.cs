using Core.Types;
using Core.Async;
using Cysharp.Threading.Tasks;

namespace Sample
{
    class AwaitSceneActionCommand : ICommandAsync<Unit, MainScreenActions>
    {
        private readonly MainScreenActionSource actionSource;

        public AwaitSceneActionCommand(MainScreenActionSource actionSource)
        {
            this.actionSource = actionSource;
        }

        public async UniTask<MainScreenActions> Execute(Unit param) => await actionSource.Task;

    }
}
