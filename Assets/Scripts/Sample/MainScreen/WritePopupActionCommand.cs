using Core;
using Core.Types;

namespace Sample
{
    class WritePopupActionCommand : ICommand<PopupActions, Unit>
    {
        private readonly IMainScreen mainScreen;

        public WritePopupActionCommand(IMainScreen mainScreen)
        {
            this.mainScreen = mainScreen;
        }

        public Unit Execute(PopupActions param)
        {
            mainScreen.SetPopup(param);
            return Unit.Default;
        }
    }
}
