using Core;
using Core.Types;

namespace Sample
{
    class IncrementActionCounterCommand : ICommand<Unit, Unit>
    {
        private readonly IMainScreen mainScreen;

        public IncrementActionCounterCommand(IMainScreen mainScreen)
        {
            this.mainScreen = mainScreen;
        }

        public Unit Execute(Unit param)
        {
            mainScreen.ActionCounter++;
            return Unit.Default;
        }
    }
}
