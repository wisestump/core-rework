using System;
using System.Threading;
using Core;
using Core.Async;
using Core.Types;
using Cysharp.Threading.Tasks;
using MessagePipe;
using VContainer.Unity;

namespace Sample
{
    class MainScreenChains : IAsyncStartable
    {
        ICommandAsync<Unit, Unit> sceneActionDispatcher;

        public MainScreenChains(
            AwaitSceneActionCommand actionAwaiter,
            IncrementActionCounterCommand increment,
            WritePopupActionCommand writePopupResult,
            LoadSceneCommand loadScene,
            ResetActionSourceCommand resetSource,
            IResourceProvider<PopupScopedCompletionSource> awaitPopup,
            IPublisher<ActivateLogicSignal> activationPublisher,
            IPublisher<DeactivateLogicSignal> deactivationPublisher,
            ScopedCancellationSource scopedCancellationSource)
        {
            // chain
            //var ignoreResult = Id<MainScreenActions>.Instance.Convert(_ => Unit.Default);
            //var activatePopupLogic = new ActionLambdaCommand<Unit>(_ =>
            //    activationPublisher.Publish(ActivateLogicSignal.Named("Popup")));
            //var deactivatePopupLogic = new ActionLambdaCommand<Unit>(_ =>
            //    deactivationPublisher.Publish(DeactivateLogicSignal.Named("Popup")));
            //var awaitPopupResult = new LambdaCommandAsync<Unit, PopupActions>(_ => awaitPopup.Get().Task);
            //sceneActionDispatcher = actionAwaiter
            //    .If(x => x == MainScreenActions.OnScreenAction)
            //    .Then(ignoreResult.Next(increment))
            //    .ElseIf(x => x == MainScreenActions.OpenPopup)
            //    .Then(
            //        Id<MainScreenActions>.Instance.Convert(_ => "Popup")
            //        .Next(loadScene)
            //        .Next(new ActionLambdaCommandAsync<Unit>(_ => UniTask.NextFrame()))
            //        .Next(activatePopupLogic)
            //        .Next(awaitPopupResult)
            //        .Next(writePopupResult)
            //        .Next(deactivatePopupLogic))
            //    .Else(ignoreResult)
            //    .Next(resetSource)
            //    .Loop(scopedCancellationSource.Token);

            // no chain
            sceneActionDispatcher = new LambdaCommandAsync<Unit, Unit>(async _ =>
            {
                while (!scopedCancellationSource.IsCancellationRequested)
                {
                    var action = await actionAwaiter.Execute(Unit.Default);
                    if (action == MainScreenActions.OnScreenAction)
                    {
                        increment.Execute(Unit.Default);
                    }
                    else if (action == MainScreenActions.OpenPopup)
                    {
                        await loadScene.Execute("Popup");
                        // TODO: this should be async signal, since activation can involve heavy actions
                        // (e.g. creating child container)
                        activationPublisher.Publish(ActivateLogicSignal.Named("Popup"));
                        var result = await awaitPopup.Get().Task;
                        writePopupResult.Execute(result);
                        deactivationPublisher.Publish(DeactivateLogicSignal.Named("Popup"));
                    }
                    resetSource.Execute(Unit.Default);
                }
                return Unit.Default;
            });
        }

        public ICommandAsync<Unit, Unit> SceneActionDispatcher => sceneActionDispatcher;

        public async UniTask StartAsync(CancellationToken cancellation)
        {
            await sceneActionDispatcher.Execute(Unit.Default);
        }
    }
}
