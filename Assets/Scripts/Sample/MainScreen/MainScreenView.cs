using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Sample
{
    class MainScreenView : MonoBehaviour
    {
        public Button OnScreenAction;
        public Button OpenPopup;
        public Button MoveToAnotherScene;

        [SerializeField] private TextMeshProUGUI text;

        public void SetActionCount(int count) => text.text = $"Counter: {count}";
        public void SetPopupResult(string text) => this.text.text = text;
    }

}
