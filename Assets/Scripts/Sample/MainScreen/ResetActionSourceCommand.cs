﻿using Core.Types;
using Core;

namespace Sample
{
    class ResetActionSourceCommand : ICommand<Unit, Unit>
    {
        private readonly MainScreenActionSource source;

        public ResetActionSourceCommand(MainScreenActionSource source)
        {
            this.source = source;
        }

        public Unit Execute(Unit param)
        {
            source.Dispose();
            source.Initialize();
            return Unit.Default;
        }
    }
}
