﻿namespace Sample
{
    enum MainScreenActions
    {
        OnScreenAction,
        OpenPopup,
        MoveToAnotherScene,
    }

}
