using System;
using UniRx;

namespace Sample
{
    interface IMainScreen
    {
        int ActionCounter { get; set; }

        void SetPopup(PopupActions actions);
    }

    interface IMainScreenReactive
    {
        IReadOnlyReactiveProperty<int> ActionCounter { get; }
        IObservable<PopupActions> PopupActions { get; }
    }

    class MainScreen : IMainScreen, IMainScreenReactive
    {
        private readonly ReactiveProperty<int> actionCounter = new ReactiveProperty<int>();
        private readonly Subject<PopupActions> popupActions = new Subject<PopupActions>();

        IReadOnlyReactiveProperty<int> IMainScreenReactive.ActionCounter => actionCounter;
        IObservable<PopupActions> IMainScreenReactive.PopupActions => popupActions;

        int IMainScreen.ActionCounter
        {
            get => actionCounter.Value;
            set => actionCounter.Value = value;
        }


        void IMainScreen.SetPopup(PopupActions actions)
        {
            popupActions.OnNext(actions);
        }
    }
}
