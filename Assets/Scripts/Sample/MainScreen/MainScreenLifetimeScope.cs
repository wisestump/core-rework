using VContainer;
using VContainer.Unity;
using Core;

namespace Sample
{
    class MainScreenLifetimeScope : LifetimeScope
    {
        public MainScreenView View;

        protected override void Configure(IContainerBuilder builder)
        {
            builder.Register<MainScreen>(Lifetime.Singleton).AsImplementedInterfaces();
            builder.RegisterComponent(View);
            builder.RegisterEntryPoint<MainScreenPresenter>();
            builder.RegisterEntryPoint<MainScreenActionSource>(Lifetime.Singleton).AsSelf();

            // TODO: implement child
            ActiveLogicScope(builder);
        }

        private void ActiveLogicScope(IContainerBuilder builder)
        {
            builder.Register<AwaitSceneActionCommand>(Lifetime.Singleton);
            builder.Register<IncrementActionCounterCommand>(Lifetime.Singleton);
            builder.Register<ResetActionSourceCommand>(Lifetime.Singleton);
            builder.Register<WritePopupActionCommand>(Lifetime.Singleton);

            builder.RegisterEntryPoint<ScopedCancellationSource>().AsSelf();

            builder.RegisterEntryPoint<MainScreenChains>();
        }
    }
}
