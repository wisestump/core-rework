using Core;
using UniRx;
using UnityEngine.UI;
using VContainer.Unity;

namespace Sample
{
    class MainScreenPresenter : ScopedPresenter, IStartable
    {
        private readonly MainScreenView view;
        private readonly MainScreenActionSource actionSource;
        private readonly IMainScreenReactive mainScreen;

        public MainScreenPresenter(MainScreenView view, MainScreenActionSource actionSource, IMainScreenReactive mainScreen)
        {
            this.view = view;
            this.actionSource = actionSource;
            this.mainScreen = mainScreen;
        }

        public void Start()
        {
            BindActionToCompletionSource(view.OnScreenAction, MainScreenActions.OnScreenAction);
            BindActionToCompletionSource(view.OpenPopup, MainScreenActions.OpenPopup);
            BindActionToCompletionSource(view.MoveToAnotherScene, MainScreenActions.MoveToAnotherScene);

            mainScreen.ActionCounter.Subscribe(view.SetActionCount).AddTo(scopedDisposables);
            mainScreen.PopupActions.Select(x => x.ToString()).Subscribe(view.SetPopupResult).AddTo(scopedDisposables);
        }

        void BindActionToCompletionSource(Button button, MainScreenActions action)
        {
            button.OnClickAsObservable().Subscribe(_ => actionSource.TrySetResult(action)).AddTo(scopedDisposables);
        }
    }
}
