using VContainer;
using VContainer.Unity;
using Core;
using System.Collections.Generic;

namespace Sample
{

    class PopupLifetimeScope : LifetimeScope
    {
        public PopupView View;

        protected override void Configure(IContainerBuilder builder)
        {
            builder.RegisterComponent(View);
            builder.RegisterLogicActivator(this, ActiveLogicScope, "Popup");
            builder.RegisterEntryPoint<PopupPresenter>();
            builder.Register<PopupScopedCompletionSource>(Lifetime.Singleton);
            builder.RegisterSingletonResourceProvider<PopupScopedCompletionSource>();
        }

        private void ActiveLogicScope(IContainerBuilder builder)
        {
            builder.RegisterLifetimeInterfaces<PopupScopedCompletionSource>();
        }
    }
}
