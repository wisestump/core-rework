using Core;
using UniRx;
using UnityEngine.UI;
using VContainer.Unity;
using MessagePipe;

namespace Sample
{
    class PopupPresenter : ScopedPresenter, IStartable
    {
        private readonly PopupView view;
        private readonly PopupScopedCompletionSource actionSource;
        private readonly ISubscriber<HardwareBackSignal> backSignal;

        public PopupPresenter(PopupView view, PopupScopedCompletionSource actionSource, ISubscriber<HardwareBackSignal> backSignal)
        {
            this.view = view;
            this.actionSource = actionSource;
            this.backSignal = backSignal;
        }

        public void Start()
        {
            BindActionToCompletionSource(view.OkButton, PopupActions.OK);
            BindActionToCompletionSource(view.CancelButton, PopupActions.Cancel);
            backSignal.Subscribe(_ => actionSource.TrySetResult(PopupActions.Cancel)).AddTo(scopedDisposables);
        }

        void BindActionToCompletionSource(Button button, PopupActions action)
        {
            button.OnClickAsObservable().Subscribe(_ => actionSource.TrySetResult(action)).AddTo(scopedDisposables);
        }
    }
}
