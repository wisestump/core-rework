using UnityEngine;
using MessagePipe;
using VContainer.Unity;
using Core;

namespace Sample
{
    class AndroidBackKeyCapturer : ITickable
    {
        private readonly IPublisher<HardwareBackSignal> backSignalPublisher;

        public AndroidBackKeyCapturer(IPublisher<HardwareBackSignal> backSignalPublisher)
        {
            this.backSignalPublisher = backSignalPublisher;
        }

        public void Tick()
        {
            // Check if Back was pressed this frame
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                backSignalPublisher.Publish(default);
            }
        }
    }
}

