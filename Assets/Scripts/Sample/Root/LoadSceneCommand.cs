using Core;
using Core.Types;
using Core.Async;
using Cysharp.Threading.Tasks;
using UnityEngine.SceneManagement;
using VContainer.Unity;

namespace Sample
{
    class LoadSceneCommand : ICommandAsync<string, Unit>
    {
        private readonly LifetimeScope parent;

        public LoadSceneCommand(LifetimeScope defaultParent)
        {
            this.parent = defaultParent;
        }

        public async UniTask<Unit> Execute(string sceneName)
        {
            var scene = SceneManager.GetSceneByName(sceneName);
            if (scene.isLoaded)
                return Unit.Default;

            // TODO: async lock
            using (LifetimeScope.EnqueueParent(parent))
            {
                await SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
            }
            return Unit.Default;
        }
    }

}
