namespace Core
{
    public struct HardwareBackSignal { }

    public struct ActivateLogicSignal
    {
        public string Name { get; private set; }
        public static ActivateLogicSignal Named(string value) => new ActivateLogicSignal() { Name = value };
    }

    public struct DeactivateLogicSignal
    {
        public string Name { get; private set; }
        public static DeactivateLogicSignal Named(string value) => new DeactivateLogicSignal() { Name = value };
    }
}
