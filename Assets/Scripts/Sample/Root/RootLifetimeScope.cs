using Core;
using MessagePipe;
using VContainer;
using VContainer.Unity;

namespace Sample
{
    public class RootLifetimeScope : LifetimeScope
    {
        protected override void Configure(IContainerBuilder builder)
        {
            var options = builder.RegisterMessagePipe();
            builder.RegisterMessageBroker<HardwareBackSignal>(options);
            builder.RegisterMessageBroker<ActivateLogicSignal>(options);
            builder.RegisterMessageBroker<DeactivateLogicSignal>(options);

            builder.Register<LoadSceneCommand>(Lifetime.Singleton).WithParameter<LifetimeScope>(this);

#if UNITY_EDITOR || UNITY_ANDROID
            builder.RegisterEntryPoint<AndroidBackKeyCapturer>();
#endif
            // TODO: move to dedicated installer
            builder.RegisterResourceLocator<PopupScopedCompletionSource>();
        }
    }
}
