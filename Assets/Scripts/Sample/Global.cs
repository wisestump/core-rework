using VContainer.Unity;
using System;

class Global 
{
    public static int Count = 0;

    public int Id { get; private set; }

    public Global()
    {
        Id = Count++;
        UnityEngine.Debug.Log($"Creating Global {Id}");
    }

    public void Dispose()
    {
        UnityEngine.Debug.Log($"Disposing {GetType().Name}");
    }

    void Start()
    {
        UnityEngine.Debug.Log($"Start {GetType().Name}");
    }

    public void PostStart()
    {
        UnityEngine.Debug.Log($"PostStart {GetType().Name}");
    }
}

class GlobalDependant : IStartable, IDisposable, IPostStartable
{
    public GlobalDependant(Global global)
    {
        UnityEngine.Debug.Log($"Global id = {global.Id}");
    }

    public void Dispose()
    {
        UnityEngine.Debug.Log($"Disposing {GetType().Name}");
    }

    public void PostStart()
    {
        UnityEngine.Debug.Log($"PostStart {GetType().Name}");
    }

    public void Start()
    {
        UnityEngine.Debug.Log($"Start {GetType().Name}");
    }
}
