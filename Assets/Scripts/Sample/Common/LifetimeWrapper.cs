using System;
using VContainer.Unity;

namespace Core
{
    class LifetimeWrapper<T> : IInitializable, IDisposable
        where T : class, IInitializable, IDisposable
    {
        private readonly T obj;

        public LifetimeWrapper(T obj)
        {
            this.obj = obj ?? throw new Exception($"Type {typeof(T).FullName} is not registered");
        }

        public void Dispose()
        {
            obj.Dispose();
        }

        public void Initialize()
        {
            obj.Initialize();
        }
    }
}
