using VContainer;
using System;
using VContainer.Unity;

namespace Core
{
    public static class VContainerExtensions
    {
        /// <summary>
        /// Registers <see cref="IResourceAssigner{T}"/> and <see cref="IResourceProvider{T}"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        public static void RegisterResourceLocator<T>(this IContainerBuilder builder)
        {
            builder.Register<ResourceLocator<T>>(Lifetime.Singleton).AsImplementedInterfaces();
        }

        /// <summary>
        /// Resolves <see cref="T"/> only once and uses resolved instance until the container is disposed.
        /// Uses <see cref="IResourceAssigner{T}"/> to set resource provider.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        public static void RegisterSingletonResourceProvider<T>(this IContainerBuilder builder)
        {
            builder
                .Register<ResourceLocator<T>.ScopedSingletonInstanceProvider>(Lifetime.Singleton)
                .As<IDisposable>()
                .AsSelf();

            builder.RegisterBuildCallback(container =>
            {
                try
                {
                    var provider = container.Resolve<ResourceLocator<T>.ScopedSingletonInstanceProvider>();
                    var providerAssigner = container.Resolve<IResourceAssigner<T>>();
                    providerAssigner.Assign(provider);
                }
                catch (VContainerException)
                {
                    throw new NullReferenceException(
                        $"ResourceLocator for type {typeof(T).FullName} not found. " +
                        $"Use {nameof(IContainerBuilder)}.{nameof(RegisterResourceLocator)} to create one.");
                }
            });
        }

        /// <summary>
        /// Allows using <see cref="ActivateLogicSignal"/>/<see cref="DeactivateLogicSignal"/> to create/dispose child Lifetime Scope.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="parent">Parent scope</param>
        /// <param name="installer">Installer for child scope</param>
        /// <param name="logicName">Name for the child scope, that's used in signals <see cref="ActivateLogicSignal"/>/<see cref="DeactivateLogicSignal"/> </param>
        public static void RegisterLogicActivator(this IContainerBuilder builder, LifetimeScope parent, Action<IContainerBuilder> installer, string logicName)
        {
            builder.RegisterEntryPoint<LogicActivator>(Lifetime.Singleton)
                .WithParameter<Func<LifetimeScope>>(() => parent.CreateChild(installer))
                .WithParameter(logicName);
        }

        /// <summary>
        /// Registers <see cref="IInitializable"/> and <see cref="IDisposable"/> interfaces for already regestered type <typeparamref name="T"/>
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="builder"></param>
        public static void RegisterLifetimeInterfaces<T>(this IContainerBuilder builder)
            where T : class, IInitializable, IDisposable
        {
            builder.RegisterEntryPoint<LifetimeWrapper<T>>();
        }
    }
}
