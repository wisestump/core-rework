using Core.Async;
using Core.Types;
using Cysharp.Threading.Tasks;
using System;

namespace Sample
{
    class LambdaCommandAsync<TP, TR> : ICommandAsync<TP, TR>
    {
        private readonly Func<TP, UniTask<TR>> func;

        public LambdaCommandAsync(Func<TP, UniTask<TR>> func)
        {
            this.func = func;
        }

        public async UniTask<TR> Execute(TP param) => await func(param);
    }

    class ActionLambdaCommandAsync<TP> : ICommandAsync<TP, Unit>
    {
        private readonly Func<TP, UniTask> func;

        public ActionLambdaCommandAsync(Func<TP, UniTask> func)
        {
            this.func = func;
        }

        public async UniTask<Unit> Execute(TP param)
        {
            await func(param);
            return Unit.Default;
        }
    }
}
