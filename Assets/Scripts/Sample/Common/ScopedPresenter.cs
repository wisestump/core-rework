﻿using System;
using UniRx;

namespace Core
{
    class ScopedPresenter : IDisposable
    {
        protected readonly CompositeDisposable scopedDisposables = new CompositeDisposable();

        public void Dispose()
        {
            scopedDisposables.Clear();
        }
    }

}
