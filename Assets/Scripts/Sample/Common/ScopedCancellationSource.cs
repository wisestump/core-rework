using VContainer.Unity;
using System;
using System.Threading;

namespace Core
{
    public class ScopedCancellationSource : IInitializable, IDisposable
    {
        CancellationTokenSource source;

        public bool IsCancellationRequested => source.IsCancellationRequested;
        public CancellationToken Token => source.Token;

        public void Dispose()
        {
            source?.Cancel();
            source?.Dispose();
            source = null;
        }

        public void Initialize()
        {
            if (source == null)
            {
                source = new CancellationTokenSource();
            }
        }
    }
}
