using System;
using Core;
using Core.Types;

namespace Sample
{
    class LambdaCommand<TP, TR> : ICommand<TP, TR>
    {
        private readonly Func<TP, TR> func;

        public LambdaCommand(Func<TP, TR> func)
        {
            this.func = func;
        }

        public TR Execute(TP param) => func(param);
    }

    class ActionLambdaCommand<TP> : ICommand<TP, Unit>
    {
        private readonly Action<TP> func;

        public ActionLambdaCommand(Action<TP> func)
        {
            this.func = func;
        }

        public Unit Execute(TP param)
        {
            func(param);
            return Unit.Default;
        }
    }
}
