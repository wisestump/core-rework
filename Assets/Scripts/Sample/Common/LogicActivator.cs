using VContainer.Unity;
using System;
using MessagePipe;

namespace Core
{
    class LogicActivator : IInitializable, IDisposable
    {
        private readonly Func<LifetimeScope> childFactory;
        private LifetimeScope child;
        private readonly string logicName;
        private readonly ISubscriber<ActivateLogicSignal> activationStream;
        private readonly ISubscriber<DeactivateLogicSignal> deactivationStream;
        private IDisposable requestListeners;

        public LogicActivator(
            Func<LifetimeScope> childFactory,
            string logicName,
            ISubscriber<ActivateLogicSignal> activationStream,
            ISubscriber<DeactivateLogicSignal> deactivationStream)
        {
            this.childFactory = childFactory;
            this.logicName = logicName;
            this.activationStream = activationStream;
            this.deactivationStream = deactivationStream;
        }

        public void Dispose()
        {
            requestListeners?.Dispose();
            requestListeners = null;
            DeactivateLogicIfActive();
        }

        public void Initialize()
        {
            requestListeners = DisposableBag.Create(
                activationStream.Subscribe(OnActivationRequested, x => x.Name == logicName),
                deactivationStream.Subscribe(OnDeactivationRequested, x => x.Name == logicName));
        }

        public void OnActivationRequested(ActivateLogicSignal signal)
        {
            if (child == null)
            {
                child = childFactory();
            }
        }

        public void OnDeactivationRequested(DeactivateLogicSignal signal)
        {
            DeactivateLogicIfActive();
        }

        private void DeactivateLogicIfActive()
        {
            if (child != null)
            {
                child.Dispose();
            }

            child = null;
        }
    }
}
