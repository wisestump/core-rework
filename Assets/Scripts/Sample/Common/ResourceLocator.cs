using System;

namespace Core
{
    /// <summary>
    /// Provides instance on request. Similar to resource locator but for a single type.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface IResourceProvider<T>
    {
        T Get();
    }

    /// <summary>
    /// Used to assign provider for type <see cref="T"/>
    /// </summary>
    /// <typeparam name="T"></typeparam>
    interface IResourceAssigner<T>
    {
        void Assign(IResourceProvider<T> provider);
    }

    class ResourceLocator<T> : IResourceAssigner<T>, IResourceProvider<T>
    {
        private IResourceProvider<T> provider;

        internal class ScopedSingletonInstanceProvider : IDisposable, IResourceProvider<T>
        {
            private T value;

            public ScopedSingletonInstanceProvider(T value)
            {
                this.value = value;
            }

            public void Dispose()
            {
                value = default;
            }

            public T Get() => value;
        }

        public T Get()
        {
            if (provider == null)
            {
                throw new NullReferenceException($"Provider for type {typeof(T).FullName} is not set.");
            }

            return provider.Get();
        }

        public void Assign(IResourceProvider<T> provider) => this.provider = provider;
    }
}
