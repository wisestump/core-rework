using VContainer.Unity;
using Cysharp.Threading.Tasks;
using System;

namespace Core
{
    class ScopedCompletionSource<T> : IInitializable, IDisposable
    {
        private UniTaskCompletionSource<T> source;

        // UniTaskCompletionSource API

        public UniTask<T> Task
        {
            get
            {
                if (source == null)
                    throw new NullReferenceException($"{GetType().FullName} is disposed or not initialized");

                return source.Task;
            }
        }

        public void TrySetCanceled(System.Threading.CancellationToken token = default) => source?.TrySetCanceled(token);
        public void TrySetException(Exception e) => source?.TrySetException(e);
        public void TrySetResult(T result) => source?.TrySetResult(result);

        // Scope API

        public void Dispose()
        {
            source?.TrySetCanceled();
            source = null;
        }

        public void Initialize()
        {
            if (source == null)
            {
                source = new UniTaskCompletionSource<T>();
            }
        }
    }
}
