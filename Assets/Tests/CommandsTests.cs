using System.Collections;
using System.Collections.Generic;
using Core;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using Core.Async;
using Core.Types;
using Cysharp.Threading.Tasks;

namespace Tests
{
    public partial class CommandsTests
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestTracking()
        {
            var original = new PassInt();

            List<string> order = new List<string>();
            var passInt = Track(new PassInt(), order, "PassInt");
            var intToUnit = Track(new IntToUnit(), order, "IntToUnit");
            var increment = Track(new Increment(), order, "Increment");
            var increment2 = Track(new Increment(), order, "Increment2");

            var resultingCommand = passInt
                .Next(increment)
                .If(x => x == 0)
                    .Then(increment)
                    .Else(intToUnit.Convert(x => 666))
                .Next(increment2);

            var result = resultingCommand.Execute(0);
            Debug.Log(result);
            Debug.Log(string.Join(", ", order));
        }

        [Test]
        public void IfElse()
        {
            var chain = new PassInt()
                .If(x => x < 0).Then(new Return<int, string>($"case 1"))
                .ElseIf(x => x == 0).Then(new Return<int, string>($"case 2"))
                .ElseIf(x => x == 1).Then(new Return<int, string>($"case 3"))
                .ElseIf(x => x == 2).Then(new Return<int, string>($"case 4"))
                .ElseIf(x => x == 3).Then(new Return<int, string>($"case 5"))
                .ElseIf(x => x == 4).Then(new Return<int, string>($"case 6"))
                .ElseIf(x => x == 5).Then(new Return<int, string>($"case 7"))
                .ElseIf(x => x == 6).Then(new Return<int, string>($"case 8"))
                .ElseIf(x => x == 7).Then(new Return<int, string>($"case 9"))
                .ElseIf(x => x == 8).Then(new Return<int, string>($"case 10"))
                .Else(new Return<int, string>($"case 11"));

            for (int i = -1; i < 10; i++)
            {
                Assert.That(chain.Execute(i) == $"case {i + 2}");
            }

            Assert.That(chain.Execute(-100) == $"case 1");
            Assert.That(chain.Execute(100) == $"case 11");
        }

        [Test]
        public void IfThenElse()
        {
            var chain = new PassInt()
                .If(x => x < 0)
                .Then(new Return<int, string>($"case 1"))
                .Else(new Return<int, string>($"case 2"));

            Assert.That(chain.Execute(-100) == $"case 1");
            Assert.That(chain.Execute(0) == $"case 2");
            Assert.That(chain.Execute(100) == $"case 2");
        }

        [Test]
        public void RecursiveChains()
        {
            RecursiveChainsAsync().Forget();
        }


        async UniTaskVoid RecursiveChainsAsync()
        {
            var sleep = new SleepCommand();
            var next = new CommandWrapper<Unit, Unit>();
            var recursive = sleep.Next(next);
            next.Value = recursive;

            // endless recursion
            //await recursive.Execute(Unit.Default);
        }
    }
}
