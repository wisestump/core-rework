using System.Collections.Generic;
using Core;
using Core.Types;
using Core.Async;
using Cysharp.Threading.Tasks;

namespace Tests
{
    public partial class CommandsTests
    {
        class TrackableCommandDecorator<T, R> : ICommand<T, R>
        {
            private readonly ICommand<T, R> command;
            private readonly string name;
            private readonly List<string> executionOrder;

            public TrackableCommandDecorator(ICommand<T, R> command, string name, List<string> executionOrder)
            {
                this.command = command;
                this.name = name;
                this.executionOrder = executionOrder;
            }

            public R Execute(T param)
            {
                executionOrder.Add(name);
                return command.Execute(param);
            }
        }

        class PassInt : ICommand<int, int>
        {
            public int Execute(int param) => param;
        }

        class IntToUnit : ICommand<int, Unit>
        {
            public Unit Execute(int param) => Unit.Default;
        }

        class Increment : ICommand<int, int>
        {
            public int Execute(int param) => param + 1;
        }

        class Return<TP, TR> : ICommand<TP, TR>
        {
            private readonly TR result;

            public Return(TR result)
            {
                this.result = result;
            }

            public TR Execute(TP param) => result;
        }

        class Print<T> : ICommand<T, T>
        {
            private readonly string message;

            public Print(string message)
            {
                this.message = message;
            }

            public T Execute(T param)
            {
                UnityEngine.Debug.Log(message);
                return param;
            }
        }

        ICommand<T, R> Track<T, R>(ICommand<T, R> command, List<string> order, string name) =>
            new TrackableCommandDecorator<T, R>(command, name, order);
    }

    class SleepCommand : ICommandAsync<Unit, Unit>
    {
        public async UniTask<Unit> Execute(Unit param)
        {
            await UniTask.Delay(1000);
            return Unit.Default;
        }
    }

    class CommandWrapper<T, R> : ICommandAsync<T, R>
    {
        public ICommandAsync<T, R> Value { get; set; }

        public async UniTask<R> Execute(T param) => await Value.Execute(param);
    }
}
